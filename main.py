# -*- coding: utf-8 -*-
import requests
import random
from googletrans import Translator

name_1 = 'Douglas'


class MeuSignoDiz():
    def __init__(self):
        self.url_lero_lero = 'https://randomwordgenerator.com/json/phrases.json'

    def main(self, name):
        phrase = self.frase()
        print("%s Seu signo do dia é: %s" % (name, phrase))

    def frase(self ):
        page = requests.get(self.url_lero_lero)
        num = random.randint(1,101)
        phrase = self.translator(page.json()['data'][num]['meaning'])
        return phrase

    def translator(self, frase):
        translator = Translator()
        ptPhrase = translator.translate(frase, dest='pt')
        return ptPhrase.text

if __name__ == "__main__":
    MeuSignoDiz().main(name_1)